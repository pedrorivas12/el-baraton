# El Bratón

> E-commerce para la Tienda El Bratón
> Sistema hecho utilizando Nuxt.js

## Pasos para compilar

``` bash
# Instalar dependencias
$ npm install

# Servidor de desarrollo con hot reloading en localhost:3000
$ npm run dev

# Compilar para produccion y lanzar servidor
$ npm run build
$ npm start

# Generar proyecto estatico
$ npm run generate
```

Para una explicacion mas detallada revisar la documentación de
[Nuxt.js](https://nuxtjs.org).

## Informacion adicional
Se desarrollo un sistema utilizando Nuxt.js que nos permite utilizar la
tecnologia de Vue.js desde el lado del servidor.

Se decidio utilizar para los estilos CSS basicos bootstrap,
Fontawesome 4.7.x para los iconos y vuex-persist para mantener el estado en las
recargas de la página.
