import Vue from 'vue'
import productsList from '../assets/products'

const getProduct = id => {
  return productsList.products.find(item => item.id === id)
}

const ProductHelper = {
  getProduct
}

Vue.prototype.$ProductHelper = ProductHelper
